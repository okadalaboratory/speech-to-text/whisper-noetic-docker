# Copyright (c) 2022 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php

# If the first argument is ...
ifneq (,$(findstring tools_,$(firstword $(MAKECMDGOALS))))
	# use the rest as arguments
	RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
	# ...and turn them into do-nothing targets
	#$(eval $(RUN_ARGS):;@:)
endif

.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[\.0-9a-zA-Z_-]+:.*?## / {printf "\033[36m%-42s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

build-cpu: ## Build Whisper [CPU] Container
	docker build --target whisper-cpu -t okdhryk/whisper-noetic:cpu -f docker/Dockerfile .
	@printf "\n\033[92mBuild Docker Image: okdhryk/whisper-noetic:cpu\033[0m\n"

build-cpu-nocache: ## Build Whisper [CPU] Container
	docker build --target whisper-cpu -t okdhryk/whisper-noetic:cpu -f docker/Dockerfile . --no-cache
	@printf "\n\033[92mBuild Docker Image: okdhryk/whisper-noetic:cpu\033[0m\n"

build-gpu: ## Build Whisper [GPU] Container
	docker build --target whisper-gpu -t okdhryk/whisper-noetic:gpu -f docker/Dockerfile . --no-cache
	@printf "\n\033[92mBuild Docker Image: okdhryk/whisper-noetic:gpu\033[0m\n"

build-gpu-nocache: ## Build Whisper [GPU] Container
	docker build --target whisper-gpu -t okdhryk/whisper-noetic:gpu -f docker/Dockerfile . --no-cache
	@printf "\n\033[92mBuild Docker Image: okdhryk/whisper-noetic:gpu\033[0m\n"

restart: stop run
