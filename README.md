# Whisper Noetic Docker

ROS1 Noetic環境でWhisperを動かす

GPU環境での動作をお勧めします。

<img src="images/Whisper-ROS.png" width="640">


# 環境
Ubuntu 20.04<br>
GPU or CPU<br>
Docker<br>

# Ubuntu20.04をホストPCとして使う
##Ubuntu20.04をインストールする
##Docker と Docker Compose　をインストールする
##NVIDIA ドライバー と NVIDIA container toolkitをインストールする
CPU版を使う場合は不要です

## Whisperを使った音声認識環境をインストールする　（このリポジトリ)
下記の通り、リポジトリをクローンします。
```
$ cd ~
$ git clone https://gitlab.com/okadalaboratory/chat/speech-to-text/whisper-noetic-docker.git
$ cd whisper-noetic-docker
```
### Dockerイメージを作成する
下記の通り、Dockerイメージを作成します。初回はしばらく時間がかかるので気長にお待ちください。

#### GPUを使用する場合
```
$ make build-gpu
```

#### GPUを使用しない場合
```
$ make build-cpu
```

# docker-composeを使った起動方法(ローカルマシンのマイクを使う)
## 英語版　English
### GPUを使用する場合
docker-compose -f compose-en.yaml up whisper-gpu
### GPUを使用しない場合
docker-compose -f compose-en.yaml up whisper-cpu

## 日本語版　Japanese
### GPUを使用する場合
docker-compose -f compose-jp.yaml up whisper-gpu
### GPUを使用しない場合
docker-compose -f compose-jp.yaml up whisper-cpu


# docker-composeを使った起動方法(リモートマシンのマイクを使う)
## 英語版　English
### GPUを使用する場合
docker-compose -f compose-en-remote.yaml up whisper-gpu
### GPUを使用しない場合
docker-compose -f compose-en-remote.yaml up whisper-cpu

## 日本語版　Japanese
### GPUを使用する場合
docker-compose -f compose-jp-remote.yaml up whisper-gpu
### GPUを使用しない場合
docker-compose -f compose-jp-remote.yaml up whisper-cpu

# 実行コマンドのオプション
      - ROS_IP=192.168.11.222　←自分のマシンのIP
      - ROS_MASTER_URI=http://192.168.11.28:11311  ← roscoreを動かすマシンのIP
    command: /bin/bash -c "source /opt/ros/noetic/setup.bash;source /overlay_ws/devel/setup.bash;roslaunch ros_speech_recognition speech_recognition_hsr.launch language:=en-US"
 
language:=en-US　　英語
language:=ja-JP　　日本語
launch_audio_capture:=true  audio_captureを起動する
launch_audio_capture:=false audio_captureは起動しない (別途自分で立ち上げる必要がある　roslaunch audio_capture capture_wave.launch audio_topic:=/audio )
audio_topic:=/audio  デフォルト　/audio/audio



# 音声認識結果の確認
Whisper Docker イメージを起動した後、別の端末から、認識結果が配信されるTopicを確認する。
```
$ docker container exec -it whisper-noetic-gpu bash
$ rostopic echo /speech_to_text
```

# 音声認識を一時休止する
```
$ rosservice call /speech_recognition/stop "{}" 
```

# 音声認識を再開する
```
$ rosservice call /speech_recognition/start "{}" 
```

